class CreateMovies < ActiveRecord::Migration[5.1]
  def change
    create_table :movies do |t|
      t.string :title
      t.string :synopsis
      t.string :poster_url
      t.string :start_date
      t.string :datetime
      t.string :end_date
      t.string :datetime

      t.timestamps
    end
  end
end
