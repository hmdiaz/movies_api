class CreateReservations < ActiveRecord::Migration[5.1]
  def change
    create_table :reservations do |t|
      t.string :full_name
      t.string :mobile_phone
      t.string :identification
      t.string :email
      t.string :reservation_date
      t.string :datetime
      t.references :movies, foreign_key: true

      t.timestamps
    end
  end
end
